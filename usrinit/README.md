usrinit
=======

Notify users about system events  
Usage: `usrinit <event> [users...]`

This tool is named `usrinit` because it was originally used to relay events
from SystemV-init. However, you should **not** use it to manage actual
long-running services. usrinit is **not** a replacement for your init system's
service management. Bad things will especially happen if you combine
usrinit-managed long-running services with systemd-logind.

## What is does
usrinit provides a simple way of notifying local users about "system events"  
(i.e. startup, shutown, start of a backup job). It does so by searching for an
"event handler script" in each user's home directory and then running that
script as that user, with the event name as its first argument.
usrinit looks at the following paths and runs the first executable script it finds:
`$HOME/bin/usrinit`, `$HOME/usrinit`, `$HOME/.usrinit`

## Options
- All options are set via environment variables
- **USRINIT_ASYNC** - If set to anything except null (empty string), usrinit
  will run handler scripts asynchronously.
- **USRINIT_TIMEOUT** - If set to anything except null (empty string), usrinit
  will kill each handler script after the specified amount of time
  (see `timeout(1)` manpage).

## Dependencies
- `sudo` (`su` is not supported due to it's varoius limitations)
- `timeout` from GNU cureutils, if using the USRINIT_TIMEOUT option
