cronwrap
========

A simple (cron-)job report wrapper  
Based on [Chuk Houpt's cronic](https://habilis.net/cronic/)

Usage: `cronwrap <command> [arguments...]`

## What it does
cronwrap is a wrapper around arbitrary commands (usually cronjobs) that sends a
notification email if the wrapped program crashes, i.e. exits with a non zero
exit code or produces output on stderr. Its main use is curing cron's chronic
spam problem: Instead of sending out an email every time the job produces *any*
output, cronwrap will only notify you if something went wrong.  
cronwrap can also be used without cron because it sends email on its own. This
is especially useful if you want to "group" conjobs but still want individual
reports for every single job.
Oh, and it also filters shell trace lines, so you can leave your script's debug
mode (`set -x`) on without getting spammed.

## Options
- All options are set via environment variables
- **CRONWRAP_PASS_EXIT_CODE** - If set to anything except null (empty string),
  cronwrap will exit with the wrapped command's exit code instead of 0
- **CRONWRAP_PASS_STDOUT** - If set to anything except null (empty string),
  cronwrap will not capture the wrapped command's standard output and instead
  pass it to it's own parent process
- **MAILTO** - Set the mail recipient address, defaults to `$(id -un)`.
  Compatible with cron's MAILTO option.

## Dependencies
- POSIX compliant `mailx`
- GNU `grep`
