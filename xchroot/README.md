xchroot
=======

Seamless cross-architecture chroot helper

Usage: `xchroot [chroot arguments...]`

## What it does
xchroot simplifies chrooting into foreign-architecture systems by automatically
bind-mounting the relevant static qemu binary into the chroot environment.
xchroot uses a bind mount instead of copying anything to the target directory
and will work with read-only filesystems if the qemu path
(`/usr/bin/qemu-${architecture}-static`) already exists in the chroot container.
It can also autodetect the chroot container's architecture, so in most
cases the only thing you need to do is prefix your chroot commandline with an x.

## A Note about command line arguments
Because xchroot attempts to support as many chroot implementations / wrappers /
alternatives as possible, it uses a very "generic" way of finding the target
directory within the its command line arguments. Cronwrap assumes that **the
first argument that identifies an existing directory and that does not start
with a dash ('-') is the target directory**. This means that, when using command line
options, you should alwas prefer long options with an equality sign between an
option and its argument, e.g. '--user=1000' instead of '-u 1000'.

## Options
- All options are set via environment variables
- **CHROOT_COMMAND** - The chroot command that should be used, e.g.
  `chroot`, `arch-chroot` or `systemd-nspawn`. Defaults to "chroot".
- **TARGET_ARCH** - The architecture of the target rootfs. If unset, xchroot
  will attempt to autodetect it.

## Dependencies
- Static qemu user mode emulation binary for the target architecture
  (`/usr/bin/qemu-${architecture}-static`)
- binfmt_misc registration for said qemu binary. On Debian based systems,
  this will be handled by the qemu-user-static package.
- `mount`
- `hexdump` if using architecture autodetection
