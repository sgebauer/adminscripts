mkcontainer
===========

Creates debian rootfs trees for systemd-nspawn, libvirt-lxc etc.

Usage: `mkcontainer <name>`

## What is does
mkcontainer creates a minimal, up-to-date debian rootfs that can be directly
used as a systemd-nspawn container or libvirt-lxc system container.
Basic configuration is done automatically, so the resulting installation will
work out of the box with systemd-nspawn, including NATed network access
thanks to systemd-networkd.

## Options
- All options are set via environment variables
- **SUITE**: Debian suite to use. Defaults to `stretch`
- **BASE_UID**: Base uid/gid for uid namespacing, or 'pick' (default) to auto
  select one. Should be a multiple of 65536. Set to two colon separated numbers
  for separate base uid and base gid.
- **ROOTDIR**: Root directory for the new container. Defaults to
  `/var/lib/machines/$1`

## Dependencies
- btrfs filesystem and the `btrfs` command line tool
- `debootstrap`
- `systemd-nspawn`

---

## Quick Start

### systemd-nspawn
```
mkcontainer mycontainer
machinectl start mycontainer
machinectl shell mycontainer /bin/bash
```

### libvirt
1. Pick a base uid (should be a multiple of 65536)
2. `BASE_UID=your_base_uid ROOTDIR=/var/lib/libvirt/images/mycontainer mkcontainer mycontainer`
3. Create your libvirt lxc system container
   * Rootfs: /var/lib/libvirt/images/mycontainer
   * Start-UID/GID: your base uid from step 1
   * UID/GID-Range: 65536
4. Start the container
5. You can get a direct root shell in your container through `machinectl shell` if using systemd-machined
