sgebauer's adminscripts
=======================

This is my collection of short and simple shell scripts for various
Linux/UNIX server configuration & maintenance problems.

All of these script aim to:
- be as POSIX compatible as possible
- be short and easy to understand
- have minimal dependencies
- be reasonably configurable via environment variables
- and of course 'do one thing and do it well'

Every script in this repository is licensed under the MIT license unless
stated otherwise in the script's README or LICENSE file.
